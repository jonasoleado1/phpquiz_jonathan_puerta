<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pregunta 6 </title>
    <link rel="stylesheet" href="estilo.css">
</head>

<body>

    <?php
    $nombre = $_COOKIE['nombre'];
    session_name("quiz");
    session_start();

    if (isset($_REQUEST['respuesta5'])) {
        $_SESSION["respuesta5"] = $_REQUEST['respuesta5'];
        if ($_REQUEST['respuesta5'] == "Sí") {
            $_SESSION['contador']++;
        }
    } else {
        header("Location: index.php");
    }
    ?>

    <form action="pregunta7.php" method="post">
        <p>¿Cuál es la capital de Francia?</p>

        <select name="respuesta6">
            <option value="londres">Londres</option>
            <option value="madrid">Madrid</option>
            <option value="paris">París</option>
            <option value="roma">Roma</option>
        </select>
        </br>
        </br>
        <button type="submit">Enviar respuesta</button>
    </form>
</body>

</html>