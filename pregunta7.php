<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pregunta 5</title>
    <link rel="stylesheet" href="css/estilo.css">
</head>

<body>

    <?php
    $nombre = $_COOKIE['nombre'];
    session_name("quiz");
    session_start();
    if (isset($_REQUEST['respuesta6'])) {
        $_SESSION["respuesta6"] = $_REQUEST['respuesta6'];
        if ($_REQUEST['respuesta6'] == "París") {
            $_SESSION['contador']++;
        }
    } else {
        header("Location: index.php");
    }
    ?>

    <form action="pregunta8.php" method="post">
        <img src="image\arcoiris7.jpg" alt="">
        <p>¿Cuál de estos colores es un primario?</p>
        <select name="respuesta7" multiple>
            <option value="rojo">Rojo</option>
            <option value="verde">Verde</option>
            <option value="azul">Azul</option>
            <option value="rosado">Rosado</option>
        </select>
        <br>
        <br>
        <button type="submit">Enviar respuesta</button>
    </form>

</body>

</html>