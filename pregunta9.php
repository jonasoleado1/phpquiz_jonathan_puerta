<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pregunta 9 </title>
    <link rel="stylesheet" href="estilo.css">
</head>

<body>

    <?php
    $nombre = $_COOKIE['nombre'];
    $respuesta = "";
    session_name("quiz");
    session_start();
    if (isset($_REQUEST['respuesta8'])) {
        $respuesta = $_REQUEST['respuesta8'];
        $_SESSION["respuesta8"] = $_REQUEST['respuesta8'];
        if ($respuesta == 42) {
            $_SESSION['contador']++;
        }
    } else {
        header("Location: index.php");
    }
    ?>

    <form action="pregunta10.php" method="post">
        <p>¿Cuál es la capital de España?</p>

        <select name="respuesta9">
            <option value="londres">Londres</option>
            <option value="madrid">Madrid</option>
            <option value="paris">París</option>
            <option value="roma">Roma</option>
        </select>
        </br>
        </br>
        <button type="submit">Enviar respuesta</button>
    </form>

</body>

</html>