<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pregunta 5</title>
    <link rel="stylesheet" href="estilo.css">
</head>

<body>

    <?php
    $nombre = $_COOKIE['nombre'];
    $respuesta = ""; //se declara variable respuesta
    session_name("quiz"); //si ya llame el archivo quiz, no se vuelve a llamar
    session_start(); //crea una sesion mediante una peticion get y post basado mediante una cookie
    if (isset($_REQUEST['respuesta7'])) {
        $_SESSION["respuesta7"] = $_REQUEST['respuesta7'];
        if ($respuesta == "rojo" || $respuesta == "verde") {
            $_SESSION['contador']++;
        }
    } else {
        header("Location: index.php");
    }
    ?>

    <form action="pregunta9.php" method="post">
        <p>¿Cuál es la respuesta a la vida, el universo y todo lo demás?</p>

        <label>Adivina un número del 1 al 100 y escribelo a continuacion:</label>
        <input type="number" name="respuesta8" id="respuesta8" min="0" max="100">

        </br>
        </br>

        <button type="submit">Enviar respuesta</button>
    </form>

</body>

</html>