<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pregunta10</title>
    <link rel="stylesheet" href="estilo.css">
</head>

<body>

    <?php
    $nombre = $_COOKIE['nombre'];
    $respuesta = "";
    session_name("quiz");
    session_start();
    if (isset($_REQUEST['respuesta9'])) {
        $respuesta = $_REQUEST['respuesta9'];
        $_SESSION["respuesta9"] = $_REQUEST['respuesta9'];
        if ($respuesta == "madrid") {
            $_SESSION['contador']++;
        }
    } else {
        header("Location: index.php");
    }
    ?>

    <form action="resultado.php" method="post">
        <p>¿Cuál es la capital de Ecuador?</p>

        <select name="respuesta10">
            <option value="londres">Londres</option>
            <option value="quito">Quito</option>
            <option value="paris">París</option>
            <option value="roma">Roma</option>
        </select>
        </br>
        </br>
        <button type="submit">Enviar respuesta</button>
    </form>

</body>

</html>